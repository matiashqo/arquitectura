package cl.forum.response.pojo;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"cCodigoUsuario",
	"cNombre",
	"cIdPerfil",
	"cEstado",
	"cUsuarioCreacion",
	"cUsuarioModificacion",
	"cUsuarioModificacion",
	"dFechaCreacion",
	"dFechaModificacion"
})

public class Usuario
{
	@JsonProperty("cCodigoUsuario")
	private String cCodigoUsuario;
	@JsonProperty("cNombre")
	private String cNombre;
	@JsonProperty("cIdPerfil")
	private String cIdPerfil;
	@JsonProperty("cEstado")
	private boolean cEstado;
	@JsonProperty("cUsuarioCreacion")
	private String cUsuarioCreacion;
	@JsonProperty("cUsuarioModificacion")
	private String cUsuarioModificacion;
	@JsonProperty("dFechaCreacion")
	private String dFechaCreacion;
	@JsonProperty("dFechaModificacion")
	private String dFechaModificacion;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	
	
	
	public Usuario(String cCodigoUsuario, String cNombre, String cIdPerfil, boolean cEstado, String cUsuarioCreacion,
			String cUsuarioModificacion, String dFechaCreacion, String dFechaModificacions) 
	{
		super();
		this.cCodigoUsuario = cCodigoUsuario;
		this.cNombre = cNombre;
		this.cIdPerfil = cIdPerfil;
		this.cEstado = cEstado;
		this.cUsuarioCreacion = cUsuarioCreacion;
		this.cUsuarioModificacion = cUsuarioModificacion;
		this.dFechaCreacion = dFechaCreacion;
	}

	public Usuario() {
		// TODO Auto-generated constructor stub
	}

	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	public String getcCodigoUsuario() {
		return cCodigoUsuario;
	}
	public void setcCodigoUsuario(String cCodigoUsuario) {
		this.cCodigoUsuario = cCodigoUsuario;
	}
	public String getcNombre() {
		return cNombre;
	}
	public void setcNombre(String cNombre) {
		this.cNombre = cNombre;
	}
	public String getcIdPerfil() {
		return cIdPerfil;
	}
	public void setcIdPerfil(String cIdPerfil) {
		this.cIdPerfil = cIdPerfil;
	}
	public boolean getcEstado() {
		return cEstado;
	}
	public void setcEstado(boolean cEstado) {
		this.cEstado = cEstado;
	}
	public String getcUsuarioCreacion() {
		return cUsuarioCreacion;
	}
	public void setcUsuarioCreacion(String cUsuarioCreacion) {
		this.cUsuarioCreacion = cUsuarioCreacion;
	}
	public String getcUsuarioModificacion() {
		return cUsuarioModificacion;
	}
	public void setcUsuarioModificacion(String cUsuarioModificacion) {
		this.cUsuarioModificacion = cUsuarioModificacion;
	}
	public String getdFechaCreacion() {
		return dFechaCreacion;
	}
	public void setdFechaCreacion(String dFechaCreacion) {
		this.dFechaCreacion = dFechaCreacion;
	}
	public String getdFechaModificacion() {
		return dFechaModificacion;
	}
	public void setdFechaModificacion(String dFechaModificacion) {
		this.dFechaModificacion = dFechaModificacion;
	}

}

