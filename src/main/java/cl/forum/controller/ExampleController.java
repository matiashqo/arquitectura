package cl.forum.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import cl.forum.repository.UsuariosRespository;
import cl.forum.response.pojo.Usuario;

@RestController
@RequestMapping("/api")
public class ExampleController {

	@Autowired
	private UsuariosRespository usuariosRespository;
	
	@GetMapping(value = "/usuarios/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ModelAndView exampleMAV(@PathVariable("id") String id) throws JsonProcessingException {

		ObjectMapper mapper = new ObjectMapper();

		/* Se realiza consulta a procedimiento almacenado */
		Usuario usuario = usuariosRespository.getUsuario(id);
		
		/* Se mapea objeto de salida usando el template 'response' */
		ModelAndView model = new ModelAndView("response");
		model.addObject("body", mapper.writeValueAsString(usuario));
		model.addObject("success", "true");
		model.addObject("code", "200");
		model.addObject("message", "Respuesta exitosa");

		return model;
	}

}
