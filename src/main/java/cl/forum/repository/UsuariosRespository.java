package cl.forum.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;

import cl.forum.response.pojo.Usuario;
import cl.forum.utils.JdbcTemplateUtils;

@Component
public class UsuariosRespository {

	private JdbcTemplateUtils jdbcUtils;

	@Autowired
	public void setJdbcUtils(JdbcTemplateUtils jdbcUtils) {
		this.jdbcUtils = jdbcUtils;
	}

	public Usuario getUsuario(String id) throws JsonProcessingException {

		Map<String, Object> params = new HashMap<>();
		params.put("pcCodigoUsuario", id);
		Map<String, Object> usuario = jdbcUtils.callStoreProcedure("sptUsuarioObtener", params);

		@SuppressWarnings("unchecked")
		List<Map<String, Object>> salida = (List<Map<String, Object>>) usuario.get("#result-set-1");

		Usuario usu = null;
		if (salida.size() > 0) {
			Map<String, Object> usuarioBD = salida.get(0);
			usu = new Usuario();
			usu.setcCodigoUsuario((String) usuarioBD.get("cCodigoUsuario"));
			usu.setcNombre((String) usuarioBD.get("cNombre"));
			usu.setcIdPerfil((String) usuarioBD.get("cIdPerfil"));
			usu.setcEstado((boolean) ((boolean) usuarioBD.get("cEstado")));
			usu.setcUsuarioCreacion((String) usuarioBD.get("cUsuarioCreacion"));
			usu.setcUsuarioModificacion((String) usuarioBD.get("cUsuarioModificacion"));
			usu.setdFechaCreacion((String) usuarioBD.get("dFechaCreacion").toString());
			usu.setdFechaModificacion((String) usuarioBD.get("dFechaModificacion").toString());
		}

		return usu;

	}

}
