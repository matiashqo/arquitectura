package cl.forum.utils;

import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcTemplateUtils {

	private SimpleJdbcCall simpleJdbcCall;

	@Autowired
	public void setDatasource(DataSource datasource) {
		this.simpleJdbcCall = new SimpleJdbcCall(datasource);
	}

	public Map<String, Object> callStoreProcedure(String procedureName, Map<String, Object> parameters) {
		simpleJdbcCall.withProcedureName(procedureName);
		MapSqlParameterSource inParams = new MapSqlParameterSource();
		if (null != parameters) {
			for (Map.Entry<String, Object> parameter : parameters.entrySet()) {
				inParams.addValue(parameter.getKey(), parameter.getValue());
			}
		}

		return simpleJdbcCall.execute(inParams);

	}

}
